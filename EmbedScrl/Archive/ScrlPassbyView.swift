//
//  ScrlPassbyView.swift
//  EmbedScrl
//
//  Created by nemOoO on 2019/1/9.
//  Copyright © 2019 nemOoO. All rights reserved.
//

import UIKit

class ScrlPassbyView: UIView {
    
    weak var responsibleScrlView: UIScrollView?
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
//        print("!!!")
//        if let responsibleView = responsibleScrlView {
//            return responsibleView
//        }
        for view in subviews.reversed() {
            if view.hitTest(point, with: event) != nil {
                return view
            }
        }
        return nil
    }
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
