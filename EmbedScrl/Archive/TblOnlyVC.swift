//
//  TblOnlyVC.swift
//  EmbedScrl
//
//  Created by nemOoO on 2019/1/9.
//  Copyright © 2019 nemOoO. All rights reserved.
//

import UIKit

class TblOnlyVC: UIViewController {

    @IBOutlet weak var testView: UIView!
    @IBOutlet weak var testView2: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var tableHeaderView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.contentInset = UIEdgeInsets(top: 200, left: 0, bottom: 0, right: 0)
        let tapGsture = UITapGestureRecognizer(target: self, action: #selector(tapped(_:)))
        tableHeaderView.addGestureRecognizer(tapGsture)
        // Observer
        table.addObserver(self, forKeyPath: #keyPath(UIScrollView.contentOffset), options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(UIScrollView.contentOffset), let value: CGPoint = change?[.newKey] as? CGPoint {
            print("\(value)")
        }
    }
    
    @objc func tapped(_ gesture: UITapGestureRecognizer) {
        let loc = gesture.location(in: tableHeaderView)
        containerView.hitTest(loc, with: nil)
        print("\(loc)")
        testView.superview === tableHeaderView ? view.addSubview(testView) : tableHeaderView.addSubview(testView)
        testView2.isHidden = !testView2.isHidden
    }
    
    @IBAction func tap(_ sender: Any) {
        print("!!!!!!")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

    
}


extension TblOnlyVC: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIden = indexPath.row % 2 == 0 ? "even" : "odd"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIden) {
            return cell
        }
        return UITableViewCell()
    }
    
    
    
}
