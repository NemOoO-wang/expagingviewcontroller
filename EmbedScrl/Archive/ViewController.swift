//
//  ViewController.swift
//  EmbedScrl
//
//  Created by nemOoO on 2019/1/9.
//  Copyright © 2019 nemOoO. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ExtentPagingScrollable {
    
    var scrollviewShowed: ((UIScrollView) -> Void) = { _ in }
    
    var scrollviewLoaded: ((UIScrollView) -> Void) = { _ in }
    
    static func instantiate() -> ViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
    }

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollviewLoaded(self.scrollView)
    }
    override func viewDidAppear(_ animated: Bool) {
        scrollviewShowed(self.scrollView)
        print("\(#function) --- ffset: \(scrollView.contentOffset)")
    }

}

extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}
