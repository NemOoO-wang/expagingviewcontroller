//
//  ExtentPagingViewController.swift
//  EmbedScrl
//
//  Created by nemOoO on 2019/1/9.
//  Copyright © 2019 nemOoO. All rights reserved.
//

import UIKit
import SnapKit
import PagingKit


/// MARK: - 执行过程解释：
/// 流程图 + UI 解释：https://www.processon.com/view/link/5c35f667e4b0f430adfbc6e4
/// 1. header + menu 放在一个 container view，container 左右滑动时放到 base view，停止左右滑动（静止或者上下滑动）时放到当前 Paging Container VC 中；
/// 2. 收到  Paging Container VC 的上下位置偏移，改变 header & menu 的高度，同步左右 VC 的 contentoffset；
/// * header + menu 的 container 的 top constraint 绑定的是 EX 的 top 而不是所在的  Paging Container VC 的 top，是为了实现弹性高度（类似微博个人主页，朋友圈主页）





// MARK: - ViewController Conformed Protocol
protocol ExtentPagingScrollable {
    /// 继承本协议的 VC init 之后本变量已经被实现，建议在 viewDidload 之后调用，回调 VC 的 scrollview
    var scrollviewLoaded: ((UIScrollView)->Void) { get set }
    var scrollviewShowed: ((UIScrollView)->Void) { get set }
}





// MARK: - Layout Protocol
protocol ExtentPagingViewControllerLayoutSource {
    // Header Height
    func heightForHeader(_ exPagingVC: ExtentPagingViewController) -> CGFloat
    // Menu Height
    func heightForMenu(_ exPagingVC: ExtentPagingViewController) -> CGFloat
    // @Optional
    func minimumHeightForExHeader(_ exPagingVC: ExtentPagingViewController) -> CGFloat
    func defaultScrollInsetTop(_ exPagingVC: ExtentPagingViewController, at Index: Int) -> CGFloat
}
extension ExtentPagingViewControllerLayoutSource {
    /// Minimum ExHeight: Default is menu height
    /// 默认最小高度为 Menu 高度（贴紧屏幕的 menu ）
    func minimumHeightForExHeader(_ exPagingVC: ExtentPagingViewController) -> CGFloat {
        return self.heightForMenu(exPagingVC)
    }
    /// 默认 scrollview 的 inset 为 0
    func defaultScrollInsetTop(_ exPagingVC: ExtentPagingViewController, at Index: Int) -> CGFloat {
        return 0.0
    }
}





// MARK: - DataSource Protocol
protocol ExtentPagingViewControllerDataSource {
    /// eg. 实现微博类似的下拉放大图片的时候，只需要添加图片 X/Y 居中，上下紧贴 HeaderView。
    func viewForHeader(_ exPagingVC: ExtentPagingViewController) -> UIView
    
    // Default Protocol
    func numberOfItemsForExPagingViewController(_ viewController: ExtentPagingViewController) -> Int
    
    // Paging Container
    func viewController(for exPagingViewController: ExtentPagingViewController, viewControllerAt index: Int) -> ExtentPagingScrollable
    
    // @ 二者实现其中之一
    // ①
    func menuTitle(_ exPagingVC: ExtentPagingViewController, for index: Int) -> String?
    // ②
    func menuCellNib(_ exPagingVC: ExtentPagingViewController) -> UINib
    func menuCell(_ exPagingVC: ExtentPagingViewController, configureIn cell: PagingMenuViewCell, for index: Int) -> PagingMenuViewCell
    
    // @ Optional
    func menuWidth(_ exPagingVC: ExtentPagingViewController, forItemAt index: Int) -> CGFloat
    func focusCellNib(_ exPagingVC: ExtentPagingViewController) -> UINib
}
extension ExtentPagingViewControllerDataSource {
    /// 默认宽度为根据 屏幕宽度/页数 算平均值
    func menuWidth(_ exPagingVC: ExtentPagingViewController, forItemAt index: Int) -> CGFloat {
        return (UIApplication.shared.keyWindow?.bounds.size.width ?? 375.0) / CGFloat(self.numberOfItemsForExPagingViewController(exPagingVC))
    }
    // 默认 menu 下划线 focus view
    func focusCellNib(_ exPagingVC: ExtentPagingViewController) -> UINib {
        return UINib(nibName: "ExtentPagingFocusView", bundle: nil)
    }
    // @ 二者实现其中之一
    // ① 
    func menuTitle(_ exPagingVC: ExtentPagingViewController, for index: Int) -> String? {
        return nil
    }
    // ②
    // 默认 menu cell
    func menuCellNib(_ exPagingVC: ExtentPagingViewController) -> UINib {
        return UINib(nibName: "ExtentPagingMenuCell", bundle: nil)
    }
    func menuCell(_ exPagingVC: ExtentPagingViewController, configureIn cell: PagingMenuViewCell, for index: Int) -> PagingMenuViewCell {
        if let cell = cell as? ExtentPagingMenuCell, let title = self.menuTitle(exPagingVC, for: index) {
            cell.titleLabel.text = title
        }
        return cell
    }
}





// MARK: - ExtentPagingViewController
class ExtentPagingViewController: UIViewController {
    
    
    // MARK: - Public Properties
    
    var exPagingDataSource: ExtentPagingViewControllerDataSource?
    var exPagingLayoutSource: ExtentPagingViewControllerLayoutSource?
    
    
    // MARK: - Private Properties
    
    // Views
    /// Container view for header & menu
    var extentHeader = UIView()
    var header: UIView = UIView()
    var menuViewController: PagingMenuViewController = {
        let menuVC = PagingMenuViewController()
        menuVC.view.backgroundColor = UIColor.white
        menuVC.cellAlignment = .center
        return menuVC
    }()
    var contentViewController: PagingContentViewController = {
        return PagingContentViewController()
    }()
    
    // Data
    let kvoContextPointer: UnsafeMutableRawPointer = UnsafeMutableRawPointer.allocate(byteCount: 4, alignment: 1)
    let menuIden = "MenuCell"
    var headerHeight: CGFloat = 150.0
    var menuHeight: CGFloat = 50.0
    var maximunExHeaderHeight: CGFloat { return headerHeight + menuHeight }
    var minimumExHeaderHeight: CGFloat?
    var currentExHeaderHeight: CGFloat?
    var shouldSyncOffset: Bool { return currentExHeaderHeight! > minimumExHeaderHeight! }
    var scrollViewNObserverPool: [(UIScrollView?, CGFloat, NSKeyValueObservation?)] = []
    var observingIndex: Int?
    
    
    // MARK: - Public Func
    
    func reloadData() {
        if let dataSource = exPagingDataSource, let layoutSource = exPagingLayoutSource {
            // Set Layout
            headerHeight = layoutSource.heightForHeader(self)
            menuHeight = layoutSource.heightForMenu(self)
            minimumExHeaderHeight = layoutSource.minimumHeightForExHeader(self)
            // Init scrl view array
            scrollViewNObserverPool = Array<(UIScrollView?, CGFloat, NSKeyValueObservation?)>(repeating: (nil, 0.0, nil), count: dataSource.numberOfItemsForExPagingViewController(self))
            // Regist Nib
            menuViewController.register(nib: dataSource.menuCellNib(self), forCellWithReuseIdentifier: menuIden)
            menuViewController.registerFocusView(nib: dataSource.focusCellNib(self))
            // Reload
            menuViewController.reloadData()
            contentViewController.reloadData()
        }
    }
    
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        deInstall()
    }
    
    func deInstall() {
        // KVO 移除观察者
        for (_, _, observer) in scrollViewNObserverPool {
            if let observer = observer {
                observer.invalidate()
            }
        }
    }
    
    /// 添加默认约束
    func setupViews() {
        // Content VC
        view.addSubview(contentViewController.view)
        addChild(contentViewController)
        contentViewController.delegate = self
        contentViewController.dataSource = self
        contentViewController.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        // Extent Header
        view.addSubview(extentHeader)
        extentHeader.snp.makeConstraints { (make) in
            make.top.lessThanOrEqualTo(view)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(currentExHeaderHeight ?? maximunExHeaderHeight).priority(.low)
        }
        // Menu VC
        extentHeader.addSubview(menuViewController.view)
        addChild(menuViewController)
        menuViewController.delegate = self
        menuViewController.dataSource = self
        menuViewController.view.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(menuHeight)
        }
        // header
        extentHeader.addSubview(header)
        header.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(menuViewController.view.snp_topMargin)
        }
    }
    
    /// ⚠️ 获取追踪的 scrollview ,同步 neighbor scrollview
    func syncScrollViewHeight() {
        // Validate
        guard let index = observingIndex else { return }
        let targetTuple = scrollViewNObserverPool[index]
        guard let targetScrollView = targetTuple.0 else { return }
        // Cal Height
        let targetDefaultInsetTop = scrollViewNObserverPool[index].1
        let targetOffsetY = targetScrollView.contentOffset.y + targetDefaultInsetTop
        // Sync
        if shouldSyncOffset {
            // Sync All：同步所有VC
            for neighborIdx in 0 ..< scrollViewNObserverPool.count where neighborIdx != index {
                if let aNeighborScrlView = scrollViewNObserverPool[neighborIdx].0 {
                    let defaultInsetTop = scrollViewNObserverPool[neighborIdx].1
                    aNeighborScrlView.setContentOffset(CGPoint(x: 0, y: targetOffsetY - defaultInsetTop), animated: false)
                }
            }
        }
        // Fix Header Height
        currentExHeaderHeight = (-targetOffsetY > minimumExHeaderHeight!) ? -targetOffsetY : minimumExHeaderHeight
        fixHeader()
    }
    
    func fixHeader() {
        extentHeader.snp.remakeConstraints { (make) in
            make.top.lessThanOrEqualTo(view)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(currentExHeaderHeight!).priority(.low)
        }
    }
    
    func fetchScrollView(at index: Int) -> UIScrollView? {
        guard let index = observingIndex else { return nil }
        return scrollViewNObserverPool[index].0
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        // 旋转屏幕会使 “make.top.lessThanOrEqualTo(view)”失效，绑定临时 constraint 避免系统 crash，屏幕旋转结束会重新绑定正确的 constraint
        extentHeader.snp.remakeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(currentExHeaderHeight!).priority(.low)
        }
    }

}


// MARK: - DataSource
extension ExtentPagingViewController: PagingMenuViewControllerDataSource, PagingContentViewControllerDataSource {
    
    // Menu Data
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        if let dataSource = exPagingDataSource {
            return dataSource.numberOfItemsForExPagingViewController(self)
        }
        return 0
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        if let dataSource = exPagingDataSource {
            return dataSource.menuWidth(self, forItemAt: index)
        }
        return 60
    }
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        if let dataSource = exPagingDataSource
        {
            let cell = viewController.dequeueReusableCell(withReuseIdentifier: menuIden, for: index)
            return dataSource.menuCell(self, configureIn: cell, for: index)
        }
        return PagingMenuViewCell()
    }
    
    // Content Data
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        if let dataSource = exPagingDataSource {
            return dataSource.numberOfItemsForExPagingViewController(self)
        }
        return 0
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        if let dataSource = exPagingDataSource, let layoutSource = exPagingLayoutSource {
            if currentExHeaderHeight == nil {
                currentExHeaderHeight = maximunExHeaderHeight
            }
            var currentVC = dataSource.viewController(for: self, viewControllerAt: index)
            currentVC.scrollviewLoaded = { [weak self] scrollView in
                if let sSelf = self {
                    // Observ all scrlView offsets
                    let observation = scrollView.observe(\.contentOffset, changeHandler: {(object, change) in
                        // Sync Offset
                        sSelf.syncScrollViewHeight()
                    })
                    // Add to ex-paging control pool: scrollViewNObserverPool
                    let defaultInsetTop = layoutSource.defaultScrollInsetTop(sSelf, at: index)
                    sSelf.scrollViewNObserverPool[index] = (scrollView, defaultInsetTop, observation)
                    // Add inset for header
                    scrollView.contentInset = UIEdgeInsets(top: defaultInsetTop + sSelf.maximunExHeaderHeight, left: 0, bottom: 0, right: 0)
                    // 如果是延迟加载出来的 VC， 使之与原来的 offset 保持同步
                    let asyncLoadInsetY = -(sSelf.currentExHeaderHeight!+defaultInsetTop)
                    currentVC.scrollviewShowed = { scrollView in
                        if index != 0 {
                            scrollView.setContentOffset(CGPoint(x: 0, y: asyncLoadInsetY), animated: false)
                        }
                    }
                }
            }
            return currentVC as! UIViewController
        }
        return UIViewController()
    }
}


// MARK: - Delegate
extension ExtentPagingViewController: PagingMenuViewControllerDelegate, PagingContentViewControllerDelegate {
    
    // Menu Delegate
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        // 同步 contentViewController
        contentViewController.scroll(to: page, animated: true)
    }
    
    // Content Delegate
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        // 同步 menuViewController
        menuViewController.scroll(index: index, percent: percent, animated: false)
    }
    
    func contentViewController(viewController: PagingContentViewController, willBeginPagingAt index: Int, animated: Bool) {
        // header 加入 self.view
        view.addSubview(extentHeader)
        fixHeader()
    }
    
    func contentViewController(viewController: PagingContentViewController, didFinishPagingAt index: Int, animated: Bool) {
        observingIndex = index
        
        // header 加入 scrollview
        if let targetScrollView = fetchScrollView(at: index) {
            targetScrollView.addSubview(extentHeader)
            syncScrollViewHeight()
        }
    }
}
