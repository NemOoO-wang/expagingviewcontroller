//
//  ExEntroVC.swift
//  EmbedScrl
//
//  Created by nemOoO on 2019/1/10.
//  Copyright © 2019 nemOoO. All rights reserved.
//

import UIKit
import PagingKit


class ExEntroVC: UIViewController {
    
    var exPagingVC: ExtentPagingViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        exPagingVC.reloadData()
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ExtentPagingViewController {
            exPagingVC = vc
            exPagingVC.exPagingDataSource = self
            exPagingVC.exPagingLayoutSource = self
        }
    }
    

}


extension ExEntroVC: ExtentPagingViewControllerLayoutSource {
    func heightForHeader(_ exPagingVC: ExtentPagingViewController) -> CGFloat {
        return 150
    }
    
    func heightForMenu(_ exPagingVC: ExtentPagingViewController) -> CGFloat {
        return 50
    }
    
}


extension ExEntroVC: ExtentPagingViewControllerDataSource {
    
    func menuTitle(_ exPagingVC: ExtentPagingViewController, for index: Int) -> String? {
        return "🌈\(index)"
    }
    
    func viewForHeader(_ exPagingVC: ExtentPagingViewController) -> UIView {
        let v = UIView()
        v.backgroundColor = UIColor.red
        return v
    }
    
    func numberOfItemsForExPagingViewController(_ viewController: ExtentPagingViewController) -> Int {
        return 3
    }
    
    func viewController(for exPagingViewController: ExtentPagingViewController, viewControllerAt index: Int) -> ExtentPagingScrollable {
        let vc = ViewController.instantiate()
        return vc
    }
    
}
