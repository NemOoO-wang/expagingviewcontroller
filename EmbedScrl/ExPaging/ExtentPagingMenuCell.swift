//
//  ExtentPagingMenuCell.swift
//  EmbedScrl
//
//  Created by nemOoO on 2019/1/14.
//  Copyright © 2019 nemOoO. All rights reserved.
//

import UIKit
import PagingKit


class ExtentPagingMenuCell: PagingMenuViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                titleLabel.textColor = .black
                titleLabel.font = UIFont.systemFont(ofSize: 16)
            } else {
                titleLabel.textColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0)
                titleLabel.font = UIFont.systemFont(ofSize: 14)
            }
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
